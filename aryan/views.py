from django.shortcuts import render, get_object_or_404
from django.views.generic import TemplateView
from .models import  *
from django.contrib.auth.models import User

class HomeView(TemplateView):
    template_name = 'index.html'

    def get(self, request):
        company=get_object_or_404(Company, id=1)
        gwwinner=Gameweekwinner.objects.all()
        winner=Winner.objects.all()

        args={
            'company':company,
            'gwwinner':gwwinner,
            'winner':winner
        }
        return render(request,self.template_name,args)

class TeamView(TemplateView):
    template_name='teams.html'

    def get(self,request):
        winner=Winner.objects.all()
        company=get_object_or_404(Company, id=1)
        teams=Teams.objects.all()
        args={
            'company':company,
            'teams':teams,
            'winner':winner
        }
        return render(request,self.template_name,args)

class AboutView(TemplateView):
    template_name='about.html'
    
    def get(self,request):
        company=get_object_or_404(Company, id=1)

        args={
            'company':company
        }
        return render(request,self.template_name,args)

class GameweekWinnerView(TemplateView):
    template_name='mods/gameweekwinner.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        gwwinner=Gameweekwinner.objects.all().order_by('-no')

        args={
            'company':company,
            'gwwinner':gwwinner,
        }
        return render(request,self.template_name,args)

class ClassicLeagueView(TemplateView):
    template_name='mods/classicleague.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        winner=ClassicLeague.objects.all()
        args={
            'company':company,
            'winner':winner
        }
        return render(request,self.template_name,args)

class RulesView(TemplateView):
    template_name='rules.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        args={
            'company':company,
        }
        return render(request,self.template_name,args)

class EliminationLeagueView(TemplateView):
    template_name='mods/elimination.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        divisiona=Division.objects.get(name="A")
        divisionb=Division.objects.get(name="B")
        divisionc=Division.objects.get(name="C")
        divisiond=Division.objects.get(name="D")
        totalmanagers=Teams.objects.count()
        eliminatedmanagers=Teams.objects.filter(elimination='eliminated').count()
        remainingmanagers=totalmanagers-eliminatedmanagers

        # for team in divisiona.team.all():
        #     print(team.name)
            
        divisions=Division.objects.all()

        args={
            'company':company,
            'divisiona':divisiona,
            'divisionb':divisionb,
            'divisionc':divisionc,
            'divisiond':divisiond,
            'totalmanagers':totalmanagers,
            'eliminatedmanagers':eliminatedmanagers,
            'remainingmanagers':remainingmanagers
        }
        return render(request,self.template_name,args)

class DivisionView(TemplateView):
    template_name='mods/division.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        divisiona=DivisionGameweekWinner.objects.filter(Division__name="A").order_by('-date')
        divisionb=DivisionGameweekWinner.objects.filter(Division__name="B").order_by('-date')
        divisionc=DivisionGameweekWinner.objects.filter(Division__name="C").order_by('-date')
        divisiond=DivisionGameweekWinner.objects.filter(Division__name="D").order_by('-date')

        args={
            'company':company,
            'divisiona':divisiona,
            'divisionb':divisionb,
            'divisionc':divisionc,
            'divisiond':divisiond
        }
        return render(request,self.template_name,args)

class IamSafeView(TemplateView):
    template_name='mods/iamsafe.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)
        divisiona=Division.objects.get(name="A")
        divisionb=Division.objects.get(name="B")
        divisionc=Division.objects.get(name="C")
        divisiond=Division.objects.get(name="D")
        totalmanagers=Teams.objects.count()
        eliminatedmanagers=Teams.objects.filter(issafe='not safe').count()
        remainingmanagers=totalmanagers-eliminatedmanagers

        # for team in divisiona.team.all():
        #     print(team.name)
            
        divisions=Division.objects.all()

        args={
            'company':company,
            'divisiona':divisiona,
            'divisionb':divisionb,
            'divisionc':divisionc,
            'divisiond':divisiond,
            'totalmanagers':totalmanagers,
            'eliminatedmanagers':eliminatedmanagers,
            'remainingmanagers':remainingmanagers
        }
        return render(request,self.template_name,args)

class RewardsView(TemplateView):
    template_name='rewards.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)

        args={
            'company':company
        }
        return render(request,self.template_name,args)

class BlogsView(TemplateView):
    template_name='blog.html'

    def get(self,request):
        args={

        }
        return render(request,self.template_name,args)

class BlogsingleView(TemplateView):
    template_name='single.html'

    def get(self,request):
        args={

        }
        return render(request,self.template_name,args)


class ContactView(TemplateView):
    template_name='contact.html'

    def get(self,request):
        company=get_object_or_404(Company, id=1)

        args={
            'company':company
        }
        return render(request,self.template_name,args)