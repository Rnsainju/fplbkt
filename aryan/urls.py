from .views import *
from django.conf.urls import url
from django.urls import include,path

app_name = 'cv'

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    path('teams', TeamView.as_view(), name='teams'),
    path('about',AboutView.as_view(),name='about'),
    path('rewards',RewardsView.as_view(),name='rewards'),
    path('gameweekwinner',GameweekWinnerView.as_view(),name='gameweekwinner'),
    path('classicleague',ClassicLeagueView.as_view(),name='classicleague'),
    path('divisionleague',DivisionView.as_view(),name='divisionleague'),
    path('eliminationleague',EliminationLeagueView.as_view(),name='eliminationleague'),
    path('iamsafe',IamSafeView.as_view(),name='iamsafe'),
    path('blogs',BlogsView.as_view(),name='blogs'),
    path('blog/',BlogsingleView.as_view(),name='blog'),
    path('rules',RulesView.as_view(),name='rules'),
    path('contact',ContactView.as_view(),name='contact'),
]

